// LOG EVERYTHING

netlifyIdentity.on('init', user => console.log('init', user));
netlifyIdentity.on('login', user => console.log('login', user));
netlifyIdentity.on('logout', () => console.log('Logged out'));
netlifyIdentity.on('error', err => console.error('Error', err));
netlifyIdentity.on('open', () => console.log('Widget opened'));
netlifyIdentity.on('close', () => console.log('Widget closed'));

// UPDATE LOGIN BUTTON TEXT

netlifyIdentity.on('init', updateAccountText);
netlifyIdentity.on('login', updateAccountText);
netlifyIdentity.on('logout', updateAccountText);

function updateAccountText(user) {
    if (user) document.getElementById("account").textContent = "Hallo, " + user.user_metadata.full_name;
    else      document.getElementById("account").textContent = "Inloggen";
}

// HIDE NETLIFY CALLOUT

netlifyIdentity.on('open', function(){
    document.getElementById("netlify-identity-widget").contentDocument.querySelector('.callOut').style.display = 'none';
});